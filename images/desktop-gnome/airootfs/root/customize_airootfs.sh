#!/bin/bash
set -e -u
usermod -s /usr/bin/bash root
cp -aT /etc/skel/ /root/
chmod 700 /root
rm -rf /usr/share/doc/* /usr/share/man/*
echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen
echo -e 'LANG=en_US.UTF-8\nLC_ADDRESS=en_US.UTF-8\nLC_IDENTIFICATION=en_US.UTF-8\nLC_MEASUREMENT=en_US.UTF-8\nLC_MONETARY=en_US.UTF-8\nLC_NAME=en_US.UTF-8\nLC_NUMERIC=en_US.UTF-8\nLC_PAPER=en_US.UTF-8\nLC_TELEPHONE=en_US.UTF-8\nLC_TIME=en_US.UTF-8' > /etc/default/locale
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
rm -rf /etc/pacman.d/mirrorlist*
curl 'https://www.archlinux.org/mirrorlist/?country=US&protocol=http' > /etc/pacman.d/mirrorlist.src
sed -i 's/#Server/Server/' /etc/pacman.d/mirrorlist.src
rankmirrors /etc/pacman.d/mirrorlist.src > /etc/pacman.d/mirrorlist
echo 'amethystlinux' > /etc/hostname 
sed -i 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub
sed -i 's/GRUB_DISTRIBUTOR="Arch"/GRUB_DISTRIBUTOR="Amethyst"/' /etc/default/grub
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet/GRUB_CMDLINE_LINUX_DEFAULT="quiet nmi_watchdog=0 radeon.si_support=0 amdgpu.si_support=1 amdgpu.cik_support=1 radeon.cik_support=0/' /etc/default/grub
sed -i 's/#SystemMaxUse=/SystemMaxUse=128/' /etc/systemd/journald.conf
sed -i 's/NAME="Arch Linux"/NAME="Amethyst Linux"/' /etc/os-release
sed -i 's/PRETTY_NAME="Arch Linux"/PRETTY_NAME="Amethyst Linux"/' /etc/os-release
sed -i 's/ID="arch"/ID="amethyst"/' /etc/os-release
sed -i 's/ANSI_COLOR="0;36"/ANSI_COLOR="0;35"/' /etc/os-release
sed -i 's/Current=/Current=minimal/' /usr/lib/sddm/sddm.conf.d/default.conf
sed -i 's/Session=/Session=gnome.desktop/' /usr/lib/sddm/sddm.conf.d/default.conf
sed -i 's/User=/User=liveuser/' /usr/lib/sddm/sddm.conf.d/default.conf
sed -i 's/#COMPRESSION="lz4"/COMPRESSION="lz4"/' /etc/mkinitcpio.conf
sed -i 's/#COMPRESSION_OPTIONS=()/COMPRESSION_OPTIONS="-9"/' /etc/mkinitcpio.conf
sed -i "s/PRESETS=('default' 'fallback')/PRESETS=('default')/" /etc/mkinitcpio.d/linux.preset
sed -i 's/fallback_/#fallback_/' /etc/mkinitcpio.d/linux.preset
echo "PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '" >> /etc/skel/.bashrc
echo "alias grep='grep --color=auto'" >> /etc/skel/.bashrc
echo "alias fgrep='fgrep --color=auto'" >> /etc/skel/.bashrc
echo "alias egrep='egrep --color=auto'" >> /etc/skel/.bashrc
echo "alias ls='ls --color=auto'" >> /etc/skel/.bashrc
echo "alias ll='ls -alF'" >> /etc/skel/.bashrc
echo "alias la='ls -A'" >> /etc/skel/.bashrc
echo "alias l='ls -CF'" >> /etc/skel/.bashrc
echo "alias sudo='sudo '" >> /etc/skel/.bashrc
echo "alias pm='yay'" >> /etc/skel/.bashrc
echo "alias sv='systemctl'" >> /etc/skel/.bashrc
pacman-key --init
pacman-key --populate archlinux
useradd -m liveuser -G wheel,audio,video,storage,power
echo "liveuser:password" | chpasswd
systemctl enable bluetooth.service
systemctl enable wpa_supplicant.service
systemctl enable NetworkManager.service 
systemctl enable haveged.service
systemctl enable gdm.service
systemctl set-default graphical.target
rm /usr/share/xsessions/gnome-classic.desktop
rm /usr/share/applications/qv4l2.desktop
rm /usr/share/applications/qvidcap.desktop
rm /usr/share/applications/avahi-discover.desktop
rm /usr/share/applications/bssh.desktop
rm /usr/share/applications/bvnc.desktop
