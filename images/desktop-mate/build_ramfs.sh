# idk if this works.
# requires 12GB+ of RAM

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

if [ -d "work" ]; then
  echo [*] Cleaning up workspace...
  rm -rf work
fi

mkdir -p work
echo [*] Creating ramdisk/tmpfs...
mount -t tmpfs -o size=8192M tmpfs work
echo [*] Starting build...
./build.sh -v
echo [*] Unmounting ramdisk/tmpfs...
umount -R work
rm -rf work
