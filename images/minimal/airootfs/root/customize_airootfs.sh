#!/bin/bash
set -e -u
usermod -s /usr/bin/bash root
cp -aT /etc/skel/ /root/
chmod 700 /root
rm -rf /usr/share/doc/* /usr/share/man/*
echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
rm -rf /etc/pacman.d/mirrorlist*
curl 'https://www.archlinux.org/mirrorlist/?country=US&protocol=http' > /etc/pacman.d/mirrorlist.src
sed -i 's/#Server/Server/' /etc/pacman.d/mirrorlist.src
rankmirrors /etc/pacman.d/mirrorlist.src > /etc/pacman.d/mirrorlist
echo 'amethystlinux' > /etc/hostname 
sed -i 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub
sed -i 's/GRUB_DISTRIBUTOR="Arch"/GRUB_DISTRIBUTOR="Amethyst"/' /etc/default/grub
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet/GRUB_CMDLINE_LINUX_DEFAULT="quiet console=tty0 tsc=reliable no_timer_check noreplace-smp kvm-intel.nested=1 rootfstype=ext4,btrfs,xfs intel_iommu=igfx_off cryptomgr.notests rcupdate.rcu_expedited=1 rcu_nocbs=0-64 nmi_watchdog=0 radeon.si_support=0 amdgpu.si_support=1 amdgpu.cik_support=1 radeon.cik_support=0  module.sig_unenforce/' /etc/default/grub
sed -i 's/#SystemMaxUse=/SystemMaxUse=128/' /etc/systemd/journald.conf
sed -i 's/NAME="Arch Linux"/NAME="Amethyst Linux"/' /etc/os-release
sed -i 's/PRETTY_NAME="Arch Linux"/PRETTY_NAME="Amethyst Linux"/' /etc/os-release
sed -i 's/ID="arch"/ID="amethyst"/' /etc/os-release
sed -i 's/ANSI_COLOR="0;36"/ANSI_COLOR="0;35"/' /etc/os-release
sed -i 's/#COMPRESSION="lz4"/COMPRESSION="lz4"/' /etc/mkinitcpio.conf
sed -i 's/#COMPRESSION_OPTIONS=()/COMPRESSION_OPTIONS="-9"/' /etc/mkinitcpio.conf
sed -i "s/PRESETS=('default' 'fallback')/PRESETS=('default')/" /etc/mkinitcpio.d/linux.preset
sed -i 's/fallback_/#fallback_/' /etc/mkinitcpio.d/linux.preset
echo "PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '" >> /etc/skel/.bashrc
echo "alias grep='grep --color=auto'" >> /etc/skel/.bashrc
echo "alias fgrep='fgrep --color=auto'" >> /etc/skel/.bashrc
echo "alias egrep='egrep --color=auto'" >> /etc/skel/.bashrc
echo "alias ls='ls --color=auto'" >> /etc/skel/.bashrc
echo "alias ll='ls -alF'" >> /etc/skel/.bashrc
echo "alias la='ls -A'" >> /etc/skel/.bashrc
echo "alias l='ls -CF'" >> /etc/skel/.bashrc
echo "alias sudo='sudo '" >> /etc/skel/.bashrc
echo "alias pm='yay'" >> /etc/skel/.bashrc
echo "alias sv='systemctl'" >> /etc/skel/.bashrc
mkdir -p /etc/systemd/system/getty@tty1.service.d/
echo -e '[Service]\nExecStart=\nExecStart=-/sbin/agetty --autologin liveuser --nozen %I 38400 linux\n' > /etc/systemd/system/getty@tty1.service.d/autologin.conf
pacman-key --init
pacman-key --populate archlinux
useradd -m liveuser -G wheel,audio,video,storage,power
echo "liveuser:password" | chpasswd
# live cd user's bashrc
echo 'tty | grep tty &> /dev/null && echo "Welcome to Amethyst ($(uname -o) $(uname -r | head -c -9 -))"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Website    https://amethystlinux.root.sx"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Support    https://amethystlinux.root.sx/support"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Docs       https://amethystlinux.root.sx/docs"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "Getting started:"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * OS Installer:         sudo installer"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * OS Configuration:     sudo sysconf"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Network Setup:        sudo nmtui"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Reboot Machine:       sudo reboot"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "Package management:"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Install a package:    pm -S <package>"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Remove a package:     pm -Rns <package>"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Update packages:      pm -Syu"' >> /home/liveuser/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /home/liveuser/.bashrc
# new users' bashrc
echo 'tty | grep tty &> /dev/null && echo "Welcome to Amethyst ($(uname -o) $(uname -r | head -c -9 -))"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Website    https://amethystlinux.root.sx"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Support    https://amethystlinux.root.sx/support"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ":: Docs       https://amethystlinux.root.sx/docs"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "Getting started:"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * OS Configuration:     sudo sysconf"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Network Setup:        sudo nmtui"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "Package management:"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Install a package:    pm -S <package>"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Remove a package:     pm -Rns <package>"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo "  * Update packages:      pm -Syu"' >> /etc/skel/.bashrc
echo 'tty | grep tty &> /dev/null && echo ""' >> /etc/skel/.bashrc
systemctl enable NetworkManager.service
systemctl enable wpa_supplicant.service
