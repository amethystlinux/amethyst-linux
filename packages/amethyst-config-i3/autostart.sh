#!/bin/bash
polybar bar1 &
compton -b &
wal -R &
xfce4-power-manager &
nm-applet &
xset s 150 300 &
xss-lock -- mantablockscreen &
sv --user start pulseaudio.service
pasystray &
